#!/bin/sh

source assets/vars
docker pull $CONTAINER_RELEASE_IMAGE || true
docker image build assets/ --cache-from $CONTAINER_RELEASE_IMAGE --tag $CONTAINER_TEST_IMAGE --tag $CONTAINER_RELEASE_IMAGE
