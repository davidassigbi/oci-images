#!/bin/sh

source assets/vars
docker image push $CONTAINER_RELEASE_IMAGE
docker image tag $CONTAINER_RELEASE_IMAGE $CONTAINER_LATEST_IMAGE
docker image push $CONTAINER_LATEST_IMAGE
