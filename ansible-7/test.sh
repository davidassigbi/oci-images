#!/bin/sh

source assets/vars
docker container run --rm $CONTAINER_TEST_IMAGE ansible --version
docker container run --rm $CONTAINER_TEST_IMAGE ansible localhost -m ping
