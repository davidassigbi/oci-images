import os


def parent_job_template():
    parent_job_template = """
workflow:
  name: 'Build, test and push images'

stages:
  - build
  - test
  - push

variables:
  # Use TLS https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#tls-enabled
  DOCKER_HOST: tcp://docker:2376
  DOCKER_TLS_CERTDIR: "/certs"


.common:
  image: docker:latest
  tags:
    - docker
  services:
    - docker:dind
  variables:
    IMAGE_DIR: <>
  rules:
    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - ${IMAGE_DIR}/assets/**/*

    - if: $CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH
      changes:
        - ${IMAGE_DIR}/*.sh
      when: manual
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - cd ${IMAGE_DIR}
    - ls -lash
    - sh build.sh
    - sh test.sh
    - sh push.sh

# prevent child pipeline trigger failure:
#   image: bash
#   stage: build
#   script:
#     - echo Prevent child pipeline trigger failure when no image directory has be modified to trigger the creation of a new job for that directory
#   rules:
#     - if: '$CI'
"""
    return parent_job_template

def child_pipeline_job_template(image):
    child_pipeline_job_template = f"""
build test push {image}:
  extends: .common
  stage: build
  variables:
    IMAGE_DIR: {image}
"""
    return child_pipeline_job_template

def get_images():
    base_path = "./"
    images = []
    for fname in os.listdir(base_path):
        path = os.path.join(base_path, fname)
        # match only non hidden directories
        if os.path.isdir(path) and not fname.startswith('.'):
            images.append(fname)

    return images

def generator():
    with open('child-pipeline-gitlab-ci.yml', 'w+') as f:
        f.write(parent_job_template())
        for image in get_images():
            f.write(child_pipeline_job_template(image))

if __name__ == "__main__":
    generator()
