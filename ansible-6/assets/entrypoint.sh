#!/bin/bash

if [ -z "${ANSIBLE_PASSWORD}" ]; then
    echo "Please set ANSIBLE_PASSWORD env var to update the ansible user password"
else
    echo "ansible:${ANSIBLE_PASSWORD}" | chpasswd
fi

exec "$@"
